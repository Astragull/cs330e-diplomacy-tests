from io import StringIO
from unittest import main, TestCase

from Diplomacy import Player, diplomacy_solve, setup, support, move, result
# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # Testing Setup function
    # ----

    def test_setup1(self):
        input = setup("C Cairo Support D")
        self.assertEqual("C", input.armyName)
        self.assertEqual("D", input.obj)
        self.assertEqual("Cairo", input.location)

    def test_setup2(self):
        input = setup("A Saigon Move Moscow")
        self.assertEqual("Moscow", input.location)
        self.assertEqual("A", input.armyName)
    
    def test_setup3(self):
        input = setup("B Tokyo Hold")
        self.assertEqual("Tokyo", input.location)
        self.assertEqual("B", input.armyName)

    
    # ----
    # Testing solve function
    # ----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")


    def test_solve2(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")


    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")


    def test_solve4(self):
        r = StringIO("A Austin Move SanAntonio\nB Dallas Support A\nC Houston Move Dallas\nD SanAntonio Support C\nE ElPaso Move Austin\nF Waco Move Austin")
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")


    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")   
    # -----
    # print
    # -----


if __name__ == "__main__": # pragma: no cover
    main()
